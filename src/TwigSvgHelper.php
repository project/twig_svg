<?php

namespace Drupal\twig_svg;

/**
 * Helper service for the twig_svg module.
 */
class TwigSvgHelper {

  /**
   * Builds an SVG.
   *
   * @param string $name
   *   The name of the icon to use.
   * @param string $title
   *   The title to apply to the icon.
   * @param array $classes
   *   Additional classes to apply to the icon.
   * @param array $attributes
   *   Additional attributes to apply to the icon.
   * @param array $wrapper_classes
   *   Additional wrapper classes to apply.
   *
   * @return array
   *   The SVG array.
   */
  public function buildSvg(string $name, string $title, array $classes, array $attributes, array $wrapper_classes): array {
    $default_classes = [
      'icon',
      'icon--' . $name,
    ];
    $classes = array_merge($default_classes, $classes);

    $default_attributes = [
      'focusable' => 'false',
    ];
    $attributes = array_merge($default_attributes, $attributes);

    return [
      '#theme' => 'twig_svg',
      '#title' => $title,
      '#classes' => implode(' ', $classes),
      '#attributes' => $attributes,
      '#name' => $name,
      '#wrapper_classes' => implode(' ', $wrapper_classes),
    ];

  }

}
