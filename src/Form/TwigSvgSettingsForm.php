<?php

namespace Drupal\twig_svg\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure twig_svg.
 */
class TwigSvgSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'twig_svg_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'twig_svg.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('twig_svg.settings');

    $form['icon_locations'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Icon locations'),
      '#default_value' => $config->get('icon_locations'),
      '#description' => $this->t('List of icon locations (relative to the site root) to include in the page footer, one location per line. Example: modules/example/example.svg'),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('twig_svg.settings')
      ->set('icon_locations', $form_state->getValue('icon_locations'))
      ->save();
  }

}
