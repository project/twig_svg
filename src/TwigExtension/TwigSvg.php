<?php

namespace Drupal\twig_svg\TwigExtension;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Adds a twig template extension to easily add an SVG.
 */
class TwigSvg extends AbstractExtension {

  /**
   * List the custom Twig functions.
   *
   * @return array
   *   The twig function.
   */
  public function getFunctions() {
    return [
      new TwigFunction('icon', [$this, 'getInlineSvg']),
    ];
  }

  /**
   * Get the name of the service listed in twig_svg.services.yml.
   *
   * @return string
   *   The service name.
   */
  public function getName() {
    return "twig_svg.twig.extension";
  }

  /**
   * Callback for the icon() Twig function.
   *
   * @param string $name
   *   The name of the icon to use.
   * @param string $title
   *   The title to apply to the icon.
   * @param array $classes
   *   Additional classes to apply to the icon.
   * @param array $attributes
   *   Additional attributes to apply to the icon.
   * @param array $wrapper_classes
   *   Additional wrapper classes to apply.
   *
   * @return array
   *   The SVG array.
   */
  public static function getInlineSvg($name, $title = '', array $classes = [], array $attributes = [], array $wrapper_classes = []) {
    $svgHelper = \Drupal::service('twig_svg.twig_svg_helper');
    return $svgHelper->buildSvg($name, $title, $classes, $attributes, $wrapper_classes);
  }

}
