# twig_svg

Icons can be included via the admin configuration form at admin/config/twig_svg/config and via an active theme.

## Admin form:
1. admin/config/twig_svg/config
2. Include file paths, relative to the site root (don't include a forward slash).

## Theme:
Ensure your icon.svg file exists within the base theme, in the following directory 'your_theme/images/icons.svg'.

## Outputting icons
To include an SVG icon in a twig template, add the following in the relevant
html.twig file:

    {{ icon('icon-name') }}

Optionally, to add a title:
    {{ icon('icon-name', 'Icon title') }}

Optionally, to add classes:
    {{ icon('icon-name', '', ['extra-class', 'another-class']) }}

Optionally, to add a title and classes:
    {{ icon('icon-name', 'Icon title', ['extra-class', 'another-class']) }}

Optionally, to add a title, classes and attributes:
    {{ icon('icon-name', 'Icon title', ['extra-class', 'another-class'], {'attribute-one': 'attribute-one-value'}) }}

Optionally, to add a title, classes, attributes and wrapper classes:
    {{ icon('icon-name', 'Icon title', ['extra-class', 'another-class'], {'attribute-one': 'attribute-one-value'}, ['wrapper-class-1',  'wrapper-class-2']) }}

Credit to https://www.lullabot.com/articles/better-svg-sprite-reuse-in-drupal-8 for the fundamentals of this.
